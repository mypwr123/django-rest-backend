from rest_framework.response import Response
from .serializers import *
from .models import *


def getProfils(request):
    profil = Profil.objects.all()                           # Pobiera wszystkie usługi
    serializer = Profil_Serializer(profil, many=True)       # Serializuje dane usług
    return Response(serializer.data)                        # Zwraca dane usług jako odpowiedź

def addProfil(request):
    serializer = Profil_Serializer(data=request.data)      # Tworzy instancję serializera dla usługi na podstawie przekazanych danych
    if serializer.is_valid():                              # Sprawdza poprawność danych
        serializer.save()                                  # Zapisuje usługę
    return Response(serializer.data)                       # Zwraca dane zapisanej usługi jako odpowiedź

def getProfil(request, pk):
    profil = Profil.objects.get(id=pk)                      # Pobiera usługę o określonym identyfikatorze (pk)
    serializer = Profil_Serializer(profil, many=False)      # Serializuje dane usługi
    return Response(serializer.data)                        # Zwraca dane usługi jako odpowiedź

def updProfil(request, pk):
    data = request.data                                         # Pobiera przekazane dane
    profil = Profil.objects.get(id=pk)                          # Pobiera usługę o określonym identyfikatorze (pk)
    serializer = Profil_Serializer(instance=profil, data=data)  # Tworzy instancję serializera dla usługi na podstawie przekazanych danych i istniejącej usługi
    
    if serializer.is_valid():                                   # Sprawdza poprawność danych
        serializer.save()                                       # Zapisuje zaktualizowaną usługę
        
    return Response(serializer.data)                            # Zwraca dane zaktualizowanej usługi jako odpowiedź

def delProfil(request, pk):
    service = Profil.objects.get(id=pk)                     # Pobiera usługę o określonym identyfikatorze (pk)
    service.delete()                                        # Usuwa usługę
    return Response('Profil został usunięty')               # Zwraca komunikat potwierdzający usunięcie usługi


def getLProfils(request):
    lprofil = LProfil.objects.all()                          
    serializer = LProfil_Serializer(lprofil, many=True)      
    return Response(serializer.data)                         

def addLProfil(request):
    serializer = LProfil_Serializer(data=request.data)      
    if serializer.is_valid():                               
        serializer.save()                                   
    return Response(serializer.data)                        

def getLProfil(request, pk):
    lprofil = LProfil.objects.get(id=pk)                    
    serializer = LProfil_Serializer(lprofil, many=False)    
    return Response(serializer.data)                        

def updLProfil(request, pk):
    data = request.data                                      
    order = LProfil.objects.get(id=pk)                          
    serializer = LProfil_Serializer(instance=order, data=data)  
    
    if serializer.is_valid():                                
        serializer.save()                                    
        
    return Response(serializer.data)                         

def delLProfil(request, pk):
    order = LProfil.objects.get(id=pk)                    
    order.delete()                                        
    return Response('LProfil został usunięty')           


def getWizytas(request):
    wizyta = Wizyta.objects.all()                       
    serializer = Wizyta_Serializer(wizyta, many=True)   
    return Response(serializer.data)                    

def addWizyta(request):
    serializer = Wizyta_Serializer(data=request.data)  
    if serializer.is_valid():                          
        serializer.save()                               
    return Response(serializer.data)                    

def getWizyta(request, pk):
    wizyta = Wizyta.objects.get(id=pk)                  
    serializer = Wizyta_Serializer(wizyta, many=False)  
    return Response(serializer.data)                    

def updWizyta(request, pk):
    data = request.data                                 
    wizyta = Wizyta.objects.get(id=pk)                  
    serializer = Wizyta_Serializer(instance = wizyta, data=data)  

    if serializer.is_valid():                              
        serializer.save()                                  
        
    return Response(serializer.data)                       

def delWizyta(request, pk):
    wizyta = Wizyta.objects.get(id=pk)                     
    wizyta.delete()                                        
    return Response('Wizyta została usunięta')
              
def getWizytaPresets(request):
    wizytapreset = WizytaPreset.objects.all()                       
    serializer = WizytaPreset_Serializer(wizytapreset, many=True)   
    return Response(serializer.data)                     

def addWizytaPreset(request):
    serializer = WizytaPreset_Serializer(data=request.data)    
    if serializer.is_valid():                             
        serializer.save()                                 
    return Response(serializer.data)                      

def getWizytaPreset(request, pk):
    wizytapreset = WizytaPreset.objects.get(id=pk)                   
    serializer = WizytaPreset_Serializer(wizytapreset, many=False)   
    return Response(serializer.data)                       

def updWizytaPreset(request, pk):
    data = request.data                                    
    wizytapreset = WizytaPreset.objects.get(id=pk)         
    serializer = WizytaPreset_Serializer(instance=wizytapreset, data=data) 
    
    if serializer.is_valid():                              
        serializer.save()                                  
        
    return Response(serializer.data)                       

def delWizytaPreset(request, pk):
    wizytapreset = WizytaPreset.objects.get(id=pk)         
    wizytapreset.delete()                                  
    return Response('WizytaPreset został usunięty')        
