from rest_framework import serializers
from .models import  User, Wizyta, WizytaPreset,LProfil,Profil

  
class Profil_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Profil
        fields = '__all__'
        
class LProfil_Serializer(serializers.ModelSerializer):
    class Meta:
        model = LProfil
        fields = '__all__'

class Wizyta_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Wizyta
        fields = '__all__'     

class WizytaPreset_Serializer(serializers.ModelSerializer):
    class Meta:
        model = WizytaPreset
        fields = '__all__'           