from django.contrib import admin
from .models import *

admin.site.register(Profil)
admin.site.register(LProfil)
admin.site.register(Wizyta)
admin.site.register(WizytaPreset)
admin.site.register(Tag)
