from rest_framework import permissions


class profilperm(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        
        if obj.owner == request.user:
            return True

        from .models import Wizyta
        wizyty = Wizyta.objects.filter(pacjent=obj)

        for wizyta in wizyty:
            if wizyta.lekarz == request.user and wizyta.pacjent == obj.owner:
                if request.method in permissions.SAFE_METHODS:
                    return True

        return False


       
class IsLProfilOwnerOrReadOnly(permissions.BasePermission):            # lrofil.owner moze wszystko reszta tylko do odczytu 

    def has_object_permission(self, request, view, obj):
        
        if request.user.is_superuser:
            return True
        
        if request.method in permissions.SAFE_METHODS:
            return True
        
        else:
            return obj.owner == request.user


class IsWizytaPresetOwnerOrReadOnly(permissions.BasePermission):         # wizytapreset.owner może wszsytko reszta tylko do odczytu     

    def has_object_permission(self, request, view, obj):
        
        if request.user.is_superuser:
            return True
        
        if request.method in permissions.SAFE_METHODS:
            return True

        if obj.owner == obj.owner.owner == request.user:
            return True

        return False

class IsWizytaLekarzOrReadOnlyForWizytaPacjent(permissions.BasePermission):       #lekarz moze wszsyko pacjent tylko do odczytu danej(swojej) wizyty      

    def has_object_permission(self, request, view, obj):

        if request.user.is_superuser:
            return True
        
        if request.method in permissions.SAFE_METHODS and obj.pacjent == request.user:
            return True
        
        if obj.lekarz == request.user:
            return True
        
        return False


