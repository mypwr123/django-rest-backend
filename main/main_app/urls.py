from django.urls import path , include 
from . import views
from django.urls import path
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),   
    
    path('wizyta/', views.Wizytas, name='wizytas'),
    path('wizyta/<str:pk>/', views.Wizyta, name='wizyta'),

    path('profil/', views.Profils, name='profils'),
    path('profil/<str:pk>/', views.Profil, name='profil'),

    path('lprofil/', views.LProfils, name='lprofils'),
    path('lprofil/<str:pk>/', views.LProfil, name='lprofil'),

    path('wizyta_preset/', views.WizytaPresets, name='wizyta_presets'),
    path('wizyta_preset/<str:pk>/', views.WizytaPreset, name='wizyta_preset'),



]


