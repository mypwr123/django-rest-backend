from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from .permissions import *
from .utils import *


@api_view(['GET', 'POST'])                                          #to przepuszcza tylko get i post
@permission_classes([IsAuthenticated,IsAdminUser])                  #to przepuszcza tylko admina 
def Wizytas(request):
    
    if request.method == 'GET':
        return getWizytas(request)
    
    if request.method == 'POST':
        return addWizyta(request)
    

@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated,IsWizytaLekarzOrReadOnlyForWizytaPacjent])           
def Wizyta(request, pk):
   
    if request.method == 'GET':
        return getWizyta(request, pk)
    
    if request.method == 'PUT':
        return updWizyta(request, pk)
    
    if request.method == 'DELETE':
        return delWizyta(request, pk)
  

@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticatedOrReadOnly])
def WizytaPresets(request):
    
    if request.method == 'GET':
        return getWizytaPresets(request)
    
    if request.method == 'POST':
        return addWizytaPreset(request)
    
  
@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticatedOrReadOnly, IsWizytaPresetOwnerOrReadOnly])
def WizytaPreset(request, pk):
   
    if request.method == 'GET':
        return getWizytaPreset(request, pk)
    
    if request.method == 'PUT':
        return updWizytaPreset(request, pk)
    
    if request.method == 'DELETE':
        return delWizytaPreset(request, pk)
    

@api_view(['GET', 'POST']) 
@permission_classes([IsAuthenticated,IsAdminUser])
def Profils(request):

    if request.method == 'GET':
        return getProfils(request)
    
    if request.method == 'POST':
        return addProfil(request)
    
    
@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated,profilperm]) 
def Profil(request, pk):
    
    if request.method == 'GET':
        return getProfil(request, pk)
    
    if request.method == 'PUT':
        return updProfil(request, pk)
    
    if request.method == 'DELETE':
        return delProfil(request, pk)
    

@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticatedOrReadOnly]) 
def LProfils(request):
    
    if request.method == 'GET':
        return getLProfils(request)
    
    if request.method == 'POST':
        return addLProfil(request)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsLProfilOwnerOrReadOnly])
def LProfil(request, pk):
    
    if request.method == 'GET':
        return getLProfil(request, pk)
    
    if request.method == 'PUT':
        return updLProfil(request, pk)
    
    if request.method == 'DELETE':
        return delLProfil(request, pk)
    